package com.example.demo;

import com.example.demo.controller.DoctorController;
import com.example.demo.entity.Doctor;
import com.example.demo.service.DoctorService;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private DoctorService doctorService;
	@Test
	void contextLoads() {
	}
	@Test
	public void testAddDoctor() {
		Doctor doctor = new Doctor();
		doctor.setName("ime 1");

		long id = doctorService.createDoctor(doctor);


	}

	@Test
	public void testGetDoctor() {

		Doctor doca = doctorService.getDoctor(1);

		assertNotNull(doca);

		assertEquals("1",doca.getName());
	}

}
