package com.example.demo.service;

import com.example.demo.entity.Doctor;
import com.example.demo.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    //zadatak 1: poigrati se sa metodama:
    //           doctorRepository.delete(doctor);
    //           doctorRepository.getById();


    //zadatak 2: prvo ucitati doktora preko ID, onda izmeniti ime, onda pozvati .save(),
    //           videti sta ce da se desi

    //zadatak 3: nalik na DoctorRepository, kreirati repo-e za ostale entitete
    //           (predstavljeni klasama u paketu entity)

    public Doctor getDoctor(long id) {
        return doctorRepository.getById(id);
    }

    public long createDoctor(Doctor doctor) {
        doctorRepository.save(doctor);

        return doctor.getId();
    }

    public void deleteDoctor(long id) {
        //podzadatak: razlika izmedju delete i deleteById
        doctorRepository.deleteById(id);

        //dummy object
//        Doctor a = new Doctor();
//        a.setId(id);
//
//        doctorRepository.delete(a);
    }
}
