package com.example.demo.service;

import com.example.demo.entity.Examination;
import com.example.demo.repository.ExaminationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExaminationService {

    //kreiranje preglead

    //ucitavanje svih pregleda od doktora

    @Autowired
    private ExaminationRepository examinationRepository;

    public void createNewExamination(Examination examination) {
//        if(...)
//            throw  ...

        examinationRepository.save(examination);
    }

    public Examination getExamination(long id) {
        return examinationRepository.getById(id);
    }

    public long createExamination(Examination examination) {
        examinationRepository.save(examination);

        return examination.getId();
    }
}
