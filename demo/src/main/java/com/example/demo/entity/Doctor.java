package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Doctor {

    @Id
    @GeneratedValue
    private long id;

    private String name;

    //1 meh: lazy fetch
    //2 meh: eager fetch

//    @OneToMany
//    private List<Examination> examinations;
}
