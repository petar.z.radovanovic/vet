package com.example.demo.controller;

import com.example.demo.entity.Doctor;
import com.example.demo.entity.Examination;
import com.example.demo.service.ExaminationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class ExaminationController {

    @Autowired
    private ExaminationService examinationService;

    @GetMapping
    public String test() { return "test";}

    @GetMapping
    public Examination getExamination(@PathVariable long examinationId) {
        return examinationService.getExamination(examinationId);
    }
    @PostMapping
    public long createDoctor(@RequestBody Examination examination) {
        return examinationService.createExamination(examination);
    }

}
