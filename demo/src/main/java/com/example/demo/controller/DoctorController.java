package com.example.demo.controller;

import com.example.demo.entity.Doctor;
import com.example.demo.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController //mi saljemo JSON-e
@RequestMapping("/doctors")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @GetMapping
    public String test() {
        return "test";
    }

    @GetMapping("/{doctorId}")
    public Doctor getDoctor(@PathVariable long doctorId) {
        return doctorService.getDoctor(doctorId);
    }

    @PostMapping
    public long createDoctor(@RequestBody Doctor doctor) {
        return doctorService.createDoctor(doctor);
    }
}
