package com.example.demo;

import com.example.demo.entity.Doctor;
import com.example.demo.entity.Examination;
import com.example.demo.service.DoctorService;
import com.example.demo.service.ExaminationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class DemoApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private ExaminationService examinationService;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DemoApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


//		Doctor d = new Doctor();
//		d.setId(2);
//
//		Examination e = new Examination();
//
//		e.setResult("nije zdrava kuca 2");
//		e.setDoctor(d);
//
//		examinationService.createNewExamination(e);
	}
}
